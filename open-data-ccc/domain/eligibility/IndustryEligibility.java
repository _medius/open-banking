package eligibility;

import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class IndustryEligibility {
    private List<String> sicCodeIncluded;
    private List<String> sicCodeExcluded;
    private List<String> notes;
    private List<OtherCodeType> otherSicCodeIncluded;
    private List<OtherCodeType> otherSicCodeExcluded;
}
