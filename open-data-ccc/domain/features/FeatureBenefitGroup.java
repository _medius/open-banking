package features;

import enums.FeatureBenefitType;
import enums.Frequency;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class FeatureBenefitGroup {
    private String name;
    private FeatureBenefitType type;
    private String benefitGroupNominalValue;
    private String fee;
    private Frequency applicationFrequency;
    private Frequency calculationFrequency;
    private List<String> notes;
    private OtherCodeType otherType;
    private OtherCodeType otherApplicationFrequency;
    private OtherCodeType otherCalculationFrequency;
    private List<FeatureBenefitItem> featureBenefitItem;
    private List<FeatureBenefitEligibility> featureBenefitEligibility;
}
