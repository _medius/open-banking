package repayment;

import enums.FeeType;
import enums.MinMaxType;
import enums.Period;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class NonRepaymentFeeChargeCap {
    private List<FeeType> feeType;
    private MinMaxType minMaxType;
    private int feeCapOccurrence;
    private String feeCapAmount;
    private Period cappingPeriod;
    private List<String> notes;
    private List<OtherCodeType> otherFeeType;
}
