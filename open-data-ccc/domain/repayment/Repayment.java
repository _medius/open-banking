package repayment;

import lombok.Data;

import java.util.List;

@Data
public class Repayment {
    private String minBalanceRepaymentRate;
    private String minBalanceRepaymentAmount;
    private List<String> notes;
    private List<NonRepaymentFeeCharges> nonRepaymentFeeCharges;
    private RepaymentAllocation repaymentAllocation;
}
