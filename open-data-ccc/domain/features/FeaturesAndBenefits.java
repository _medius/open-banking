package features;

import lombok.Data;

import java.util.List;

@Data
public class FeaturesAndBenefits {
    private List<FeatureBenefitGroup> featureBenefitGroup;
    private List<FeatureBenefitItem> featureBenefitItem;
}
