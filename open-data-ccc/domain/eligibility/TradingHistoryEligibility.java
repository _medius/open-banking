package eligibility;

import enums.MinMaxType;
import enums.Period;
import enums.TradingEligibilityType;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class TradingHistoryEligibility {
    private TradingEligibilityType tradingType;
    private MinMaxType minMaxType;
    private String amount;
    private boolean indicator;
    private String textual;
    private Period period;
    private List<String> notes;
    private OtherCodeType otherTradingType;
}
