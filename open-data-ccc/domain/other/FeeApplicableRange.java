package other;

import lombok.Data;

@Data
public class FeeApplicableRange {
    private String minimumAmount;
    private String maximumAmount;
    private String minimumRate;
    private String maximumRate;
}
