package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum SalesAccessChannels {
    CABR("Branch", "Customer can visit a branch to open the card account"),
    CACC("CallCentre", "Customer can call a call centre to open a card account"),
    CALE("Post", "Customer can open a card account via post"),
    CAON("Online", "Customer can open a card account online"),
    CARM("RelationshipManager", "Customer can ask a relationship manager to open a card account");

    public final String codeName;
    public final String description;

    SalesAccessChannels(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static SalesAccessChannels fromName(String codeName) {
        for (SalesAccessChannels salesAccessChannels : SalesAccessChannels.values()) {
            if (salesAccessChannels.codeName.equals(codeName)) {
                return salesAccessChannels;
            }
        }
        throw new IllegalArgumentException("No such SalesAccessChannels.");
    }
}
