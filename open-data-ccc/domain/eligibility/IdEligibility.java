package eligibility;

import lombok.Data;

import java.util.List;

@Data
public class IdEligibility {
    private String url;
    private List<String> notes;
}
