package domain.codes;

public enum BranchTypeCode {
	BRMB("Mobile"),
	BRPY("Physical");

	private String value;

	BranchTypeCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
