package domain;

import domain.wrappers.DecimalDegree;

import lombok.Data;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Data
@Embeddable
public class GeographicCoordinates {

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "latitude", nullable = false))
	private DecimalDegree latitude;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "longitude", nullable = false))
	private DecimalDegree longitude;
}
