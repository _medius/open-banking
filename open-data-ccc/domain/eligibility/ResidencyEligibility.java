package eligibility;

import enums.ResidencyType;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class ResidencyEligibility {
    private ResidencyType residencyType;
    private String residencyIncluded;
    private List<String> notes;
    private OtherCodeType otherResidencyType;
}
