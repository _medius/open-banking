package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum OfficerType {
    OFDR("Director", "Officer is a director of the company"),
    OFOR("Owner", "Officer is a company owner/shareholder"),
    OFOT("Other", "Provide other officer type"),
    OFPR("Partner", "Officer is a partner within a partnership"),
    OFSC("SignificantControl",
            "Covers any eligibility criteria where there is a requirement for persons of significant control.");

    public final String codeName;
    public final String description;

    OfficerType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static OfficerType fromName(String codeName) {
        for (OfficerType officerType : OfficerType.values()) {
            if (officerType.codeName.equals(codeName)) {
                return officerType;
            }
        }
        throw new IllegalArgumentException("No such OfficerType.");
    }
}
