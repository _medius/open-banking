package other;

import lombok.Data;

@Data
public class OtherCodeType {
    private String code;
    private String name;
    private String description;
}
