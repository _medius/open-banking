package features;

import enums.EligibilityType;
import enums.Period;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class FeatureBenefitEligibility {
    private String name;
    private String description;
    private EligibilityType type;
    private String amount;
    private boolean indicator;
    private String textual;
    private Period period;
    private List<String> notes;
    private OtherCodeType otherType;
}
