package domain;

import java.util.Set;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "availabilities")
public class Availability {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(optional = false)
	private StandardAvailability standardAvailability;

	@OneToMany
	private Set<NonStandardAvailability> nonStandardAvailabilities;
}
