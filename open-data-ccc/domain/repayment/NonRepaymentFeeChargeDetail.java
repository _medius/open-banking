package repayment;

import enums.FeeFrequency;
import enums.FeeType;
import enums.InterestRateType;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class NonRepaymentFeeChargeDetail {
    private FeeType feeType;
    private boolean negotiableIndicator;
    private String feeAmount;
    private String feeRate;
    private InterestRateType feeRateType;
    private FeeFrequency applicationFrequency;
    private FeeFrequency calculationFrequency;
    private List<String> notes;
    private List<OtherFeeType> otherFeeType;
    private List<OtherCodeType> otherFeeRateType;
    private List<OtherCodeType> otherApplicationFrequency;
    private List<OtherCodeType> otherCalculationFrequency;
}
