package domain;

import domain.codes.AccessibilityCode;
import domain.codes.BranchTypeCode;
import domain.codes.CustomerSegmentCode;
import domain.codes.ServiceAndFacilityCode;

import java.util.List;
import java.util.Set;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "branches")
public class Branch {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 35, nullable = false)
	private String identification;

	@Column(length = 35, nullable = false)
	private String sequenceNumber;

	@Column(length = 140)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(length = 4, nullable = false)
	private BranchTypeCode type;

	@ElementCollection
	@CollectionTable(name = "sort_codes",
			joinColumns = @JoinColumn(name = "branch_id"))
	@Column(length = 8)
	private Set<String> sortCode;

	@Column
	private String photo;

	@ElementCollection(targetClass = CustomerSegmentCode.class)
	@CollectionTable(name = "customer_segments", joinColumns = @JoinColumn(name = "branch_id"))
	@Enumerated(EnumType.STRING)
	@Column(name = "customer_segment", length = 4, nullable = false)
	@Size(min = 1)
	private Set<CustomerSegmentCode> customerSegment;

	@ElementCollection(targetClass = ServiceAndFacilityCode.class)
	@CollectionTable(name = "service_and_facilities", joinColumns = @JoinColumn(name = "branch_id"))
	@Enumerated(EnumType.STRING)
	@Column(name = "service_and_facility", length = 4, nullable = false)
	private Set<ServiceAndFacilityCode> serviceAndFacility;

	@ElementCollection(targetClass = AccessibilityCode.class)
	@CollectionTable(name = "accessibilities", joinColumns = @JoinColumn(name = "branch_id"))
	@Enumerated(EnumType.STRING)
	@Column(name = "accessibility", length = 4, nullable = false)
	private Set<AccessibilityCode> accessibility;

	@Column(length = 2000)
	private String note;

	@OneToMany
	@JoinColumn(name = "id", referencedColumnName = "id")
	private Set<OtherCustomerSegment> otherCustomerSegment;

	@OneToMany
	@JoinColumn(name = "id", referencedColumnName = "id")
	private Set<OtherAccessibility> otherAccessibility;

	@OneToMany
	@JoinColumn(name = "id", referencedColumnName = "id")
	private Set<OtherServiceAndFacility> otherServiceAndFacility;

	@OneToOne
	private Availability availability;

	@OneToMany
	@JoinColumn(name = "id", referencedColumnName = "id")
	private List<ContactInfo> contactInfo;

	@OneToOne(optional = false)
	private PostalAddress postalAddress;
}
