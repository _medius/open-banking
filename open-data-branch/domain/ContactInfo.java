package domain;

import domain.codes.ContactTypeCode;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contact_info")
public class ContactInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length = 4, nullable = false)
	private ContactTypeCode contactType;

	@Column(nullable = false)
	private String contactContent;

	@Column(length = 70)
	private String contactDescription;

	@Embedded
	private OtherContactType otherContactType;
}
