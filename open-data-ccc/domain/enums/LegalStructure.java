package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum LegalStructure {
    LSCI("CIO", "Charitable Incorporated Organisation"),
    LSCS("ClubSociety", "Club or Society"),
    LSCY("Charity", "Charity"),
    LSLD("Ltd", "Limited Company"),
    LSLG("LBG", "Company limited by Guarantee"),
    LSLP("LLP", "Limited Liability Partnership"),
    LSOT("Other", "Provide other legal structure"),
    LSPP("Partnership", "Partnership"),
    LSST("SoleTrader", "Sole trader"),
    LSTT("Trust", "Non-personal Trust");

    public final String codeName;
    public final String description;

    LegalStructure(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static LegalStructure fromName(String codeName) {
        for (LegalStructure legalStructure : LegalStructure.values()) {
            if (legalStructure.codeName.equals(codeName)) {
                return legalStructure;
            }
        }
        throw new IllegalArgumentException("No such LegalStructure.");
    }
}
