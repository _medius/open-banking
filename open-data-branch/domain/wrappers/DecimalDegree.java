package domain.wrappers;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class DecimalDegree {

	@Column(name = "value")
	private final Double value;

	public DecimalDegree(Double value) {
		this.value = value;
	}
}
