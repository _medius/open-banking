package eligibility;

import enums.CreditScoring;
import lombok.Data;

import java.util.List;

@Data
public class CreditCheckEligibility {
    private CreditScoring scoringType;
    private List<String> notes;
}
