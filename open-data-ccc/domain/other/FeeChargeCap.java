package other;

import enums.FeeType;
import enums.MinMaxType;
import enums.Period;
import lombok.Data;

import java.util.List;

@Data
public class FeeChargeCap {
    private List<FeeType> feeType;
    private MinMaxType minMaxType;
    private int feeCupOccurrence;
    private String feeCapAmount;
    private Period cappingPeriod;
    private List<String> notes;
    private OtherCodeType otherFeeType;
}
