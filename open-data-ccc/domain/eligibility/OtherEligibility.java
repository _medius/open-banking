package eligibility;

import enums.EligibilityType;
import enums.Frequency;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class OtherEligibility {
    private String name;
    private String description;
    private EligibilityType type;
    private String amount;
    private boolean indicator;
    private String textual;
    private Frequency period;
    private List<String> notes;
    private OtherCodeType otherType;
}
