package other;

import lombok.Data;

import java.util.List;

@Data
public class OtherFeeCharges {
    private List<FeeChargeDetail> feeChargeDetail;
    private List<FeeChargeCap> feeChargeCap;
}
