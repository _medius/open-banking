package core;

import enums.CardScheme;
import enums.Period;
import enums.SalesAccessChannels;
import enums.ServicingAccessChannels;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class CoreProduct {
    private String productUrl;
    private String productDescription;
    private String tscAndCsUrl;
    private String maxDailyCardWithdrawalLimit;
    private String minCreditLimit;
    private String maxCreditLimit;
    private int maxPurchaseInterestFreeLengthDays;
    private List<SalesAccessChannels> salesAccessChannels;
    private List<ServicingAccessChannels> servicingAccessChannels;
    private List<CardScheme> cardScheme;
    private boolean contactlessIndicator;
    private String periodicFee;
    private Period periodicFeePeriod;
    private String apr;
    private List<String> notes;
    private List<OtherCodeType> otherCardScheme;
}
