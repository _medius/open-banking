import core.CoreProduct;
import eligibility.Eligibility;
import enums.MarketingState;
import enums.Period;
import features.FeaturesAndBenefits;
import lombok.Data;
import other.OtherFeeCharges;
import repayment.Repayment;

import java.time.LocalDate;
import java.util.List;

@Data
public class CccMarketingState {
    private String identification;
    private String predecessorId;
    private MarketingState marketingState;
    private LocalDate firstMarketedDate = LocalDate.of(1900, 1,1);
    private LocalDate lastMarketedDate = LocalDate.of(9999, 12,31);
    private int stateTenureLength;
    private Period stateTenurePeriod;
    private List<String> notes;
    private Repayment repayment;
    private Eligibility eligibility;
    private FeaturesAndBenefits featuresAndBenefits;
    private OtherFeeCharges otherFeeCharges;
    private CoreProduct coreProduct;
}
