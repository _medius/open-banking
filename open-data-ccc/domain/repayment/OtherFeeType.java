package repayment;

import enums.FeeCategory;
import lombok.Data;

@Data
public class OtherFeeType {
    private String code;
    private FeeCategory feeCategory;
    private String name;
    private String description;
}
