import lombok.Data;

import java.util.Set;

@Data
public class PostalAddress {
    private Set<String> Ardressline;
    private String BuildingNumber;
    private String StreetName;
    private String TownName;
    private Set<CountrySubDivision> countrySubDivision;
    private String Country;
    private String PostCode;
    private GeoLocation geoLocation;
}
