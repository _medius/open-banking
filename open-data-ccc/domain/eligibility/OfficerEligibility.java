package eligibility;

import enums.OfficerType;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class OfficerEligibility {
    private OfficerType officerType;
    private int maxAmount;
    private int minAmount;
    private List<String> notes;
    private OtherCodeType otherOfficerType;
}
