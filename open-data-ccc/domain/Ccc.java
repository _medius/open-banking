import enums.ProductSegment;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class Ccc {
    private String name;
    private String identification;
    private List<ProductSegment> segment;
    private List<OtherCodeType> otherSegment;
    private List<CccMarketingState> cccMarketingState;
}
