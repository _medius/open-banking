package other;

import enums.FeeCategory;
import enums.FeeFrequency;
import enums.FeeType;
import enums.InterestRateType;
import lombok.Data;
import repayment.OtherFeeType;

import java.util.List;

@Data
public class FeeChargeDetail {
    private FeeCategory feeCategory;
    private FeeType feeType;
    private boolean negotiableIndicator;
    private boolean includedInPeriodicFeeIndicator;
    private String feeAmount;
    private String feeRate;
    private InterestRateType feeRateType;
    private FeeFrequency applicationFrequency;
    private FeeFrequency calculationFrequency;
    private List<String> notes;
    private OtherCodeType otherFeeCategory;
    private OtherFeeType otherFeeType;
    private OtherCodeType otherFeeRateType;
    private OtherCodeType otherApplicationFrequency;
    private OtherCodeType otherCalculationFrequency;
    private FeeApplicableRange feeApplicableRange;
}
