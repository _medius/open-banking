package features;

import enums.FeatureBenefitType;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class FeatureBenefitItem {
    private String identification;
    private FeatureBenefitType type;
    private String name;
    private String amount;
    private boolean indicator;
    private String textual;
    private List<String> notes;
    private OtherCodeType otherType;
    private List<FeatureBenefitEligibility> featureBenefitEligibility;
}
