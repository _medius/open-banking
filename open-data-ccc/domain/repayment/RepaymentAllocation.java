package repayment;

import lombok.Data;

import java.util.List;

@Data
public class RepaymentAllocation {
    private List<String> notes;
}
