# OPEN BANKING

[![pipeline status](https://gitlab.com/rd_samara/open-banking/badges/master/pipeline.svg)](https://gitlab.com/rd_samara/open-banking/commits/master)
[![coverage report](https://gitlab.com/rd_samara/open-banking/badges/master/coverage.svg)](https://gitlab.com/rd_samara/open-banking/commits/master)

## What is Open Banking?
Open Banking is the secure way to give providers access to your financial information.
It opens the way to new products and services that could help customers 
and small to medium-sized businesses get a better deal. 
It could also give you a more detailed understanding of your accounts, 
and help you find new ways to make the most of your money.

Get ready for a world of apps and websites, where you can choose new financial products and services from providers 
regulated by the Financial Conduct Authority (FCA) and European equivalents.

## Glossary

[Open Banking glossary](https://www.openbanking.org.uk/about-us/glossary/)
