package repayment;

import lombok.Data;

import java.util.List;

@Data
public class NonRepaymentFeeCharges {
    private List<NonRepaymentFeeChargeDetail> nonRepaymentFeeChargeDetail;
    private List<NonRepaymentFeeChargeCap> nonRepaymentFeeChargeCap;
}
