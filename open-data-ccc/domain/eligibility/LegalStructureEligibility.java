package eligibility;

import enums.LegalStructure;
import lombok.Data;
import other.OtherCodeType;

import java.util.List;

@Data
public class LegalStructureEligibility {
    private LegalStructure legalStructure;
    private List<String> notes;
    private OtherCodeType otherLegalStructure;
}
